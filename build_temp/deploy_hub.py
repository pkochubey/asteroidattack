# -*- coding: utf-8 -*-
#
# (C) 2001-2012 Marmalade. All Rights Reserved.
#
# This document is protected by copyright, and contains information
# proprietary to Marmalade.
#
# This file consists of source code released by Marmalade under
# the terms of the accompanying End User License Agreement (EULA).
# Please do not use this program/source code before you have read the
# EULA and have agreed to be bound by its terms.
#
import deploy_config

config = deploy_config.config
cmdline = deploy_config.cmdline
mkb = deploy_config.mkb
mkf = deploy_config.mkf

assets = deploy_config.assets

class HubConfig(deploy_config.DefaultConfig):
    assets = assets["Default"]
    config = [ur"C:/Users/Pavel/Documents/AsteroidAtack/resources/common.icf", ur"C:/Users/Pavel/Documents/AsteroidAtack/resources/app.icf"]
    name = ur"AsteroidAtack"
    caption = ur"AsteroidAtack"
    provider = ur"PKochubey"
    copyright = ur"(C) PKochubey"
    version = [0, 0, 1]
    wp81_extra_res = [ur"C:/Users/Pavel/Documents/AsteroidAtack/../../../../marmalade/7.4/extensions/s3efacebook/third-party/facebook_wp81_lib/Facebook.Client"]
    pass

default = HubConfig()
