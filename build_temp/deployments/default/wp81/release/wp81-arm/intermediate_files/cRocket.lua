require("class")
require("cRocketPath")

cRocket = inheritsFrom(baseClass);

function cRocket:new(x,y)
  local obj = cRocket:create()
  cRocket:init(obj);
  
  local centerDisplay = director.displayWidth/2;
  
  obj.path = cRocketPath:new(centerDisplay, 0, x, y);
  obj.point = 1;
  
  local k = (x-centerDisplay)/y;
  obj.sprite.rotation = math.atan(k)*180/math.pi;
  
  return obj
end;

function cRocket:init(obj)
  obj.sprite = director:createSprite({
      x = director.displayWidth/2,
      y = 0, 
      xScale = 0.5,
      yScale = 0.5,
      xAnchor = 0.5,
      yAnchor = 0.5,
      source = "textures/rocket.png"
  });

  physics:addNode(obj.sprite, {isSensor = true, type="static"})
end

function cRocket:update()
  self.sprite.x = self.path.px[self.point];
  self.sprite.y = self.path.py[self.point];
  
  self.point = self.point + 1;
end

function cRocket:remove()

end
