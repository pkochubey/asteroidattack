require("class");

cAsteroidPath = inheritsFrom(baseClass);

function cAsteroidPath:new(p0x, p0y, p1x, p1y, p2x, p2y)
  local obj = cAsteroidPath:create();
  
  local px = {};
  local py = {};
  
  for i=0, 1, 0.001 do
    local t_0 = math.pow((1 - i),2);
    local t_1 = 2*i*(1-i);
    local t_2 = math.pow(i,2);
    
    table.insert(px, t_0*p0x + t_1*p1x + t_2*p2x)
    table.insert(py, t_0*p0y + t_1*p1y + t_2*p2y)
  end;
  
  obj.px = px;
  obj.py = py;
  return obj;
end;