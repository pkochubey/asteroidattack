require("class")
require("cAsteroid")
require("cRocket")

cAsteroidField = inheritsFrom(baseClass);

local asteroids = {};
local rockets = {};
local countAsteroid = 0;

function cAsteroidField:new(countA, countU)
  local obj = cAsteroidField:create()
  cAsteroidField:init(countA);
  return obj
end;

function cAsteroidField:init(countA)
  for i = 1, countA do
    table.insert(asteroids, cAsteroid:new(math.random(-200,director.displayWidth+200),director.displayHeight+math.random(100,director.displayHeight)));
  end
end

function cAsteroidField:update()
  for key,value in pairs(asteroids) do
    value:update();
    
    if value:getY() < -100 then
      value:remove();
      asteroids[key] = nil;
    end;
  end
  
  for key,value in pairs(rockets) do
    value:update();
  end
end

function cAsteroidField:startNewRocket(x,y)
  table.insert(rockets, cRocket:new(x,y));
end