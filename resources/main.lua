require("cAsteroidField")
require("mobdebug").start()

local asteroidField = cAsteroidField:new(10,0)

local update = function(event)
    asteroidField:update();
end;

local startRocket = function(event)
  if event.phase == "began" then
      asteroidField:startNewRocket(event.x, event.y);
   end
end

system:addEventListener("touch", startRocket)
system:addEventListener("update", update)