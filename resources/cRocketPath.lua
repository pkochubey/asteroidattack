require("class");

cRocketPath = inheritsFrom(baseClass);

function cRocketPath:new(p0x, p0y, p1x, p1y)
  local obj = cRocketPath:create();
  
  local px = {};
  local py = {};
  
  for i=0, 1, 0.01 do
    local t_0 = (1 - i);
    local t_1 = i;
    
    table.insert(px, t_0*p0x + t_1*p1x)
    table.insert(py, t_0*p0y + t_1*p1y)
  end;
  
  obj.px = px;
  obj.py = py;
  return obj;
end;