require("class")
require("cRocketPath")

cRocket = inheritsFrom(baseClass);

function cRocket:new(x,y,bodyCollision)
  local obj = cRocket:create()
  cRocket:init(obj);
  
  local centerDisplay = director.displayWidth/2;
  
  obj.path = cRocketPath:new(centerDisplay, 0, x, y);
  obj.point = 1;
  
  local k = (x-centerDisplay)/y;
  obj.sprite.rotation = math.atan(k)*180/math.pi;
  
  obj.sprite:addEventListener("collision", bodyCollision);
  
  return obj
end;

function cRocket:init(obj)
  obj.sprite = director:createSprite({
      x = director.displayWidth/2,
      y = 0, 
      xScale = 0.5,
      yScale = 0.5,
      xAnchor = 0.5,
      yAnchor = 0.5,
      source = "textures/rocket.png"
  });

  obj.sprite.name = "r" .. math.random(0,10000);

  physics:addNode(obj.sprite, {isSensor = true});
  physics.debugDraw = true;
end

function cRocket:update()
  if self.path.px[self.point] == nil then
    self.sprite.x = self.sprite.x + self.dx;
    self.sprite.y = self.sprite.y + self.dy;
    
    return;
  end

  if self.path.px[self.point+1] == nil then
    self.dx = self.path.px[self.point-1] - self.path.px[self.point-2] 
    self.dy = self.path.py[self.point-1] - self.path.py[self.point-2] 
  end
  
  self.sprite.x = self.path.px[self.point];
  self.sprite.y = self.path.py[self.point];
  
  self.point = self.point + 1;
end

function cRocket:getX()
  return self.sprite.x;
end

function cRocket:getY()
  return self.sprite.y;
end

function cRocket:getName()
  return self.sprite.name;
end

function cRocket:remove()
  self.sprite = self.sprite:removeFromParent();
end
