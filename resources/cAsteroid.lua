require("class")
require("cAsteroidPath")

cAsteroid = inheritsFrom(baseClass);

function cAsteroid:new(dx, dy, bodyCollision)
  local obj = cAsteroid:create()
  cAsteroid:init(obj, dx, dy);
  
  obj.path = cAsteroidPath:new(dx,dy,300,500,math.random(0, director.displayWidth),-200);
  obj.point = 1;
  obj.timer = math.random(0,60);
  
  obj.sprite:addEventListener("collision", bodyCollision);
  return obj
end;

function cAsteroid:init(obj, dx, dy)
  obj.sprite = director:createSprite({
      x = dx,
      y = dy, 
      xAnchor = 0.5,
      yAnchor = 0.5,
      xScale = 0.5,
      yScale = 0.5,
      source = "textures/a10000.png"
  });
  
  obj.sprite.name = "a" .. dx .. dy;
  
  physics:addNode(obj.sprite, {isSensor=true})
  physics.debugDraw = true;
  
  tween:to(obj.sprite, { rotation=360, time=3, mode="repeat" });
end

function cAsteroid:update()
   if self.timer > 0 then
      self.timer = self.timer - 1;
      return;
   end
   
   self.sprite.y = self.path.py[self.point];
   self.sprite.x = self.path.px[self.point];
   
   self.point = self.point + 1;
end

function cAsteroid:getY()
  return self.sprite.y;
end

function cAsteroid:getName()
  return self.sprite.name;
end

function cAsteroid:remove()
  self.sprite = self.sprite:removeFromParent();
end
