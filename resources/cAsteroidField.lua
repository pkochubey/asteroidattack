require("class")
require("cAsteroid")
require("cRocket")

cAsteroidField = inheritsFrom(baseClass);

local asteroids = {};
local rockets = {};
local countAsteroid = 0;

function cAsteroidField:new(countA, countU)
  local obj = cAsteroidField:create();
  cAsteroidField:init(countA);
  return obj
end;

function cAsteroidField:init(countA)
  for i = 1, countA do
    local asteroid = cAsteroid:new(math.random(-200,director.displayWidth+200),director.displayHeight+math.random(100,director.displayHeight),self.bodyCollision);
    
    table.insert(asteroids, asteroid);
  end
end

function cAsteroidField:update()
  for key,value in pairs(asteroids) do
    value:update();
    
    if value:getY() < -100 then
      value:remove();
      asteroids[key] = nil;
    end;
  end
  
  for key,value in pairs(rockets) do
    value:update();
    
    if value:getY() > director.displayHeight or value:getX() > director.displayWidth or value:getX() < 0 then
      value:remove();
      rockets[key] = nil;
    end;
  end
end

function cAsteroidField:startNewRocket(x,y)
  local roket = cRocket:new(x,y,self.bodyCollision);
  table.insert(rockets, roket);
end

cAsteroidField.bodyCollision = function(event)
  if event.phase == "began" then
    
    local nameA = string.sub(event.nodeA.name,1,1);
    local nameB = string.sub(event.nodeB.name,1,1);
    
    if nameA == nameB then
      return;
    end
    
    for key,value in pairs(asteroids) do
      if value:getName() == event.nodeA.name then
        value:remove();
        asteroids[key] = nil;
      end;
    end
    
    for key,value in pairs(rockets) do
      if value:getName() == event.nodeB.name then
        value:remove();
        rockets[key] = nil;
      end;
    end
  end
end